import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

//Import rootReducer để tạo strore
import rootReducer from './reducers';

//Import hàm để tạo store cho redux
import { createStore } from 'redux';

//Là 1 component kết nối giữa react và redux
import { Provider } from 'react-redux';

//Hàm để tạo store
const store = createStore(rootReducer);

//Component cần bao ngoài app để có thể đọc state từ store
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
    
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
