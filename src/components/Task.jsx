
import { Button, Grid, List, ListItem, TextField } from "@mui/material";
import { Container } from "@mui/system"
import { useDispatch, useSelector } from "react-redux";
import { addTaskClickAction, inputTaskNameAction, toggleTaskAction } from "../actions/taskAction";

const Task = () => {
    //Khai báo hàm dispatch sự kiện tới redux store
    const dispatch = useDispatch();
    const { inputString, taskList } = useSelector((reduxData) => {
        return reduxData.taskReducer;
    })

    const inputChangeHandler = (event) => {
        dispatch(inputTaskNameAction(event.target.value));
    }

    const addTaskClickHandler = () => {
        dispatch(addTaskClickAction());
    }

    const onClickToggleItem = (index) => {
        dispatch(toggleTaskAction(index));
    }
    return (
        <Container>
            {/*Là 1 hàng*/}
            <Grid container spacing={2} mt={3} alignItems="center">
                {/*Là cột*/}
                <Grid item md={9}>
                    <TextField label="Text" variant="outlined" placeholder="Input text here" fullWidth value={inputString} onChange={inputChangeHandler}/>
                </Grid>
                <Grid item md={3}>
                    <Button variant="contained" onClick={addTaskClickHandler}>Add Task</Button>
                </Grid>
                <Grid item>
                    <List dense={false}>
                        {
                            taskList.map((element, index) => {
                                return (
                                    <ListItem key={index} style={{color: element.status ? "green" : "red"}} onClick={() => onClickToggleItem(index)}>
                                        {index + 1}. {element.taskName} 
                                    </ListItem>
                                )
                            })
                        }
                    </List>
                </Grid>
            </Grid>
        </Container>
    )
}

export default Task;