import { combineReducers } from "redux";
//Root reducers

//Khai báo các reducer con
import taskReducer from "./taskReducer";

//Tạo 1 root reducer
const rootReducer = combineReducers({
    taskReducer,
    //Các reducer con nếu có
});

export default rootReducer;