import { ADD_TASK_CLICK, INPUT_TASK_NAME, TOGGLE_STATUS_TASK } from "../constants/taskConstant";

//Định nghĩ task khởi tạo cho task
//const initialState = {
//   inputString: "Test",
//    taskList: [{
//       taskName: "Task 1",
//        status: true
//   }]
//}
const initialState = {
    inputString: "",
    taskList: []
}
//Định nghĩa khai báo taskReducer
const taskReducer = (state = initialState, action) => {
    //Update state
    console.log(action);
   switch (action.type) {
    case INPUT_TASK_NAME:
        state.inputString = action.payload;
        break;
    case ADD_TASK_CLICK:
        state.taskList.push({
            taskName: state.inputString,
            status: false
        })
        state.inputString = "";
        break;
    case TOGGLE_STATUS_TASK:
        const index = action.payload;

        state.taskList[index].status = !state.taskList[index].status;
    default:
        break;
   }
    //...
    return { ...state };
}
export default taskReducer;