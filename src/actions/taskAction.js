import { ADD_TASK_CLICK, INPUT_TASK_NAME, TOGGLE_STATUS_TASK } from "../constants/taskConstant"
export const inputTaskNameAction = (inputValue) => {
    return {
        type: INPUT_TASK_NAME,
        payload: inputValue
    }
}

export const addTaskClickAction = () => {
    return {
        type: ADD_TASK_CLICK,
    }
}

export const toggleTaskAction = (taskIndex) => {
    return {
        type: TOGGLE_STATUS_TASK,
        payload: taskIndex
    }
}